<?php

namespace App\API\Migrations;

use App\API\Migrations\AbstractMigrations;

require_once 'vendor/autoload.php';

class Migrations20191020 extends AbstractMigrations
{
	protected $sql = "
	CREATE TABLE destination.country(
    id TINYINT AUTO_INCREMENT PRIMARY KEY,
    name varchar(100) not null,
    city_id TINYINT unsigned,
    CONSTRAINT nom_contrainte
    FOREIGN KEY (city_id)
        REFERENCES destination.city(id));

 INSERT INTO destination.country
        VALUES
            (NULL, 'France', 1),
            (NULL, 'Japon', 2),
            (NULL, 'Royaume unis', 3);
	";
}
