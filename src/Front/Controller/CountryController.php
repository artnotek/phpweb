<?php

namespace App\Front\Controller;

use App\Front\Controller\AbstractController;
use App\Front\Repository\CountryRepository;
use App\Front\Repository\CityRepository;

class CountryController extends AbstractController
{

	private $countryRepository;
	// private $cityRepository;

  public function __construct(CountryRepository $countryRepository)
  {
      $this->countryRepository = $countryRepository;
			// $this->cityRepository = $cityRepository;
  }

	public function index(array $uriVars = [])
	{
		var_dump($uriVars);
		$recupCountry = $this->countryRepository->findAll();
		$this->render('country/index', $this->countryRepository->findAll());



		// $this->render('country/index', [
		// 	// 'date' => $date->format('d/m/Y')
		// 	'country' => $this->countryRepository->findAll()
		// 	// 'cities' => $this->cityRepository->findAll()
		// ]);

	}
}
